package util;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author FWIN01152 : Satyanarayana Reddy
 */

public class KapTest_Student_Activation {
	private RemoteWebDriver driver;
	ExtentReports report;
	SoftAssert assertion = new SoftAssert();

	public KapTest_Student_Activation(RemoteWebDriver driver) 
	{
		this.driver = driver;
	}

	public boolean kapteststudentactivation(RemoteWebDriver driver) throws Exception {
		boolean isstudentactivated = false;


		driver.get("https://www.easymobilerecharge.com/signmeup.php");
		Thread.sleep(5000);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		WebDriverWait WaitTool = new WebDriverWait(driver, 30l);
		WaitTool.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='email']")));
		
		//Enter mail id
		driver.findElement(By.xpath("//input[@name='email']")).sendKeys("satyanarayana_reddy@fulcrumww.com");
		
		//Enter confirm email
		driver.findElement(By.xpath("//input[@name='cemail']")).sendKeys("satyanarayana_reddy@fulcrumww.com");
		
		//Enter password
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("satya.jel");
		
		//Reenter password
		driver.findElement(By.xpath("//input[@name='password2']")).sendKeys("satya.jel");
		
		//Enter first name
		driver.findElement(By.xpath("//input[@name='firstname']")).sendKeys("satya");
		
		//Enter Last name
		driver.findElement(By.xpath("//input[@name='lastname']")).sendKeys("reddy");

		//Enter mobile number
		driver.findElement(By.xpath("//input[@name='mobile']")).sendKeys("9527397068");
		Thread.sleep(5000);
		
		//select date of birth
		new Select(driver.findElement(By.xpath("//select[@name='dd']"))).selectByIndex(1);
		Thread.sleep(1500);
		
		new Select(driver.findElement(By.xpath("//select[@name='mm']"))).selectByIndex(2);
		Thread.sleep(1500);
		
		new Select(driver.findElement(By.xpath("//select[@name='yyyy']"))).selectByIndex(2);
		Thread.sleep(1500);
		
		//Enter address1
	    driver.findElement(By.xpath("//input[@name='address1']")).sendKeys("fulcrum");
				
		//Enter address2
		driver.findElement(By.xpath("//input[@name='address2']")).sendKeys("fulcrum");
				
		//Enter address3
		driver.findElement(By.xpath("//input[@name='city']")).sendKeys("pune");
		Thread.sleep(1500);
		
		//select state
		new Select(driver.findElement(By.xpath("//select[@name='state']"))).selectByIndex(2);
		Thread.sleep(1500);
		
		//select country
		new Select(driver.findElement(By.xpath("//select[@name='country']"))).selectByIndex(2);
		Thread.sleep(1500);

		//select zip code
		driver.findElement(By.xpath("//input[@name='zipcode']")).sendKeys("411057");
		Thread.sleep(1500);
		
		//selct terms and conditions
		driver.findElement(By.xpath("//input[@name='agree']")).click();
		Thread.sleep(5000);

		isstudentactivated = true;
		return isstudentactivated;
	}
}
