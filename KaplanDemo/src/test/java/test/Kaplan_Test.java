package test;

import org.testng.annotations.Test;

import base.SetupGrid;
import util.KapTest_Student_Activation;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import java.net.MalformedURLException;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterTest;

public class Kaplan_Test 
{
public RemoteWebDriver driver;

  @org.testng.annotations.Parameters({"Browser","Platform","version","NodeUrl"})
  @BeforeTest(alwaysRun=true)
  public void beforeTest(String Browser, String Platform, String version, String NodeUrl) throws Exception, MalformedURLException 
  {
	  Thread.sleep(1000);
	  SetupGrid gs= new SetupGrid();
	  driver=gs.getDriverInstance(Browser, Platform, version, NodeUrl);
  }
  
  @Test(priority=1)
	public void kapteststudentactivation() throws Exception
	{
		
		KapTest_Student_Activation kaptestpage = new KapTest_Student_Activation(driver);
		Thread.sleep(2000);
		kaptestpage.kapteststudentactivation(driver);
	}

  @AfterTest
  public void afterTest() throws Exception 
  {
	  Thread.sleep(3000);
	  driver.quit();
  }

}
