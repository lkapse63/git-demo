package base;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.junit.BeforeClass;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeTest;

public class SetupGrid 
{


//ThreadLocal will keep local copy of driver
//public ThreadLocal<RemoteWebDriver> dr = new ThreadLocal<RemoteWebDriver>();
private static Actions act;
public RemoteWebDriver driver;
public boolean clickflag;
public RemoteWebDriver getDriverInstance( String Browser, String Platform, String version, String NodeUrl) throws MalformedURLException
{

DesiredCapabilities caps = new DesiredCapabilities();
if(Browser.equalsIgnoreCase("Firefox"))
{
	//caps = new FirefoxOptions().setProfile(new FirefoxProfile()) .addTo(DesiredCapabilities.firefox());
	 // caps.setCapability("acceptInsecureCerts", true);
	caps=DesiredCapabilities.firefox();
	//caps.setBrowserName("Mozilla Firefox");
	//FirefoxOptions options = new FirefoxOptions();
	//options.setBinary("C:\\Program Files\\Mozilla Firefox\\firefox.exe");
	//caps.setCapability("firefox_binary", "C:\\Program Files\\Mozilla Firefox\\firefox.exe");
	//caps.setCapability("moz:firefoxOptions", options);
	caps.setCapability("marionette", true);
	caps.setPlatform(org.openqa.selenium.Platform.WINDOWS);
	driver= new RemoteWebDriver(new URL(NodeUrl), caps);
	//dr.set(driver);
	driver.manage().deleteAllCookies();
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	return driver;
	
}
if(Browser.equalsIgnoreCase("Chrome"))
{
	caps=DesiredCapabilities.chrome();
	//caps.setBrowserName("Google Chrome");
	caps.setPlatform(org.openqa.selenium.Platform.WINDOWS);
	driver= new RemoteWebDriver(new URL(NodeUrl), caps);
	//dr.set(driver);
	driver.manage().deleteAllCookies();
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	return driver;
}

if(Browser.equalsIgnoreCase("IE"))
{
	 caps = DesiredCapabilities.internetExplorer();
	 caps.setPlatform(org.openqa.selenium.Platform.WINDOWS);
	caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
	caps.setCapability("ignoreProtectedModeSettings", true);
	caps.setBrowserName("internet explorer");
	driver= new RemoteWebDriver(new URL(NodeUrl), caps);
	//dr.set(driver);
	driver.manage().deleteAllCookies();
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	driver.navigate().refresh();
	return driver;
}
 return driver;


}
public static Actions setupActions(WebDriver driver) 
{
	act = new Actions(driver);
	return act;
}

/*public void setWebDriver(RemoteWebDriver driver) 
{
    dr.set(driver);
}*/


}
